<html>
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <table id="maintable" style="width:100%; text-align:right;border-collapse:collapse;"><tr>
    <td width="90%" id="col_livestats" style="vertical-align:top;">
        <div>Phase: <span id="phase">?</span></div>
        <div>Total damage dealt: <span id="damagedealt">?</span></div>
        <div>Kills: <span id="kills">?</span></div>
        <div>Headshots: <span id="headshots">?</span></div>
        <div>Max kill distance: <span id="maxkilldist">?</span></div>
        <button id="alignButton">Swap Alignment</button>
    </td><td id="col_map" width="10%;">
        <canvas style="border:1px solid #000;" id="mapcanvas"><p>lol i guess you're using a browser from the 70s or something</p></canvas>
    </td>
    </tr></table>
    <script>
        var minPixelsToAnimate = 1;

        var myName = getUrlPlayerName();
        var curPhase  = "lobby";
        var curMap    = "unknown";
        var curFlightPath = undefined;
        var stnum  = 0;
        var prevLocs = [];

        var curDamageDealt = 0;
        var curKills = 0;
        var curHeadshots = 0;
        var curMaxKillDist = 0;

        var myLocX = 0;
        var myLocY = 0;

        var calcFlightPath = undefined;
        var calcLandingLines = undefined;

        var canvas = document.getElementsByTagName('canvas')[0];
        canvas.width  = Math.min(window.innerWidth, window.innerHeight)-20;
        canvas.height = Math.min(window.innerWidth, window.innerHeight)-20;
        var ctx = canvas.getContext("2d");
        var img = new Image();
        img.onload = drawMap;

        function drawPerpLine(d, x1, y1, x2, y2) {
            var angle = Math.atan2(y2 - y1, x2 - x1);

            var perpAngle = angle + (Math.PI / 2);
            var newx1 = x1 + d*Math.cos(perpAngle);
            var newy1 = y1 + d*Math.sin(perpAngle);
            var newx2 = x2 + d*Math.cos(perpAngle);
            var newy2 = y2 + d*Math.sin(perpAngle);
            
            ctx.setLineDash([5,5]);
            ctx.strokeStyle = "#FFF";
            ctx.beginPath();
                ctx.moveTo(
                    map_to_canvas(curMap, newx1),
                    map_to_canvas(curMap, newy1),
                );
                ctx.lineTo(
                    map_to_canvas(curMap, newx2),
                    map_to_canvas(curMap, newy2),
                );
                
            ctx.stroke();
            ctx.setLineDash([]);
        }

        function drawSolid(path) {
            ctx.setLineDash([]);
            ctx.beginPath();
                ctx.strokeStyle = "#FFF";
                ctx.moveTo(
                    map_to_canvas(curMap, path.start.x),
                    map_to_canvas(curMap, path.start.y)
                );
                ctx.lineTo(
                    map_to_canvas(curMap, path.end.x),
                    map_to_canvas(curMap, path.end.y)
                );
            ctx.stroke();
        }

        function drawDashed(path) {
            ctx.setLineDash([5,5]);
            ctx.beginPath();
                ctx.strokeStyle = "#FFF";
                ctx.moveTo(
                    map_to_canvas(curMap, path.start.x),
                    map_to_canvas(curMap, path.start.y)
                );
                ctx.lineTo(
                    map_to_canvas(curMap, path.end.x),
                    map_to_canvas(curMap, path.end.y)
                );
            ctx.stroke();
            ctx.setLineDash([]);
        }

        function drawFlightPaths() {
            if (calcFlightPath) {
                drawSolid(calcFlightPath);
            }
            if (calcLandingLines && curPhase === "aircraft") {
                for (var i=0; i<calcLandingLines.length; i++) {
                    drawDashed(calcLandingLines[i]);
                }
            }
        }

        function drawMap() {
            ctx.drawImage(img, 0, 0, canvas.scrollWidth, canvas.scrollHeight);
            drawFlightPaths();
        }

        function updateKills(kills) {
            curKills = kills;
            $("#kills").text(curKills);
        }

        function updateHeadshots(headshots) {
            curHeadshots = headshots;
            $("#headshots").text(curHeadshots);
        }

        function updateDamageDealt(damagedealt) {
            curDamageDealt = damagedealt;
            $("#damagedealt").text(curDamageDealt);
        }

        function updateMaxKillDist(maxkilldist) {
            curMaxKillDist = Math.round(maxkilldist)/100;
            $("#maxkilldist").text(curMaxKillDist + "m");
        }

        function map_size(mapname) {
            if (mapname == "sanhok")  return 4000*1.02;
            if (mapname == "vikendi") return 6000*1.02;
            else return 8000*1.02;
        }
        function map_size_unfixed(mapname) {
            if (mapname == "sanhok")  return 4000;
            if (mapname == "vikendi") return 6000;
            else return 8000;
        }

        function map_to_canvas(mapname, ord) {
            return (ord * canvas.scrollWidth)/map_size(mapname);
        }

        function map_to_canvas_unfixed(mapname, ord) {
            return (ord * canvas.scrollWidth)/map_size_unfixed(mapname);
        }

        function canvas_to_map(mapname, ord) {
            return (ord * map_size(mapname))/canvas.scrollWidth;
        }

        function updatePhase(phase) {
            console.log("Changing phase to " + phase);
            curPhase = phase;
            $("#phase").text(phase);
        }

        function updateMap(map) {
            console.log("Changing map to " + map);
            curMap = map;
            img.src = "maps/getmap.php?map="+curMap+"&size="+canvas.scrollWidth;
        }

        function isString(v) {
            return (typeof v === 'string' || v instanceof String);
        }

        function updateFlightPath(fpath) {
            if (fpath) {
                if (isString(fpath.start)) {
                    fpath.start = JSON.parse(fpath.start);
                    fpath.start.x = parseFloat(fpath.start.x);
                    fpath.start.y = parseFloat(fpath.start.y);
                    fpath.end   = JSON.parse(fpath.end);
                    fpath.end.x = parseFloat(fpath.end.x);
                    fpath.end.y = parseFloat(fpath.end.y);
                }
                curFlightPath = fpath;
                recalculateFlightPath(curFlightPath);
            }
        }

        function calcOffsetPath(fp, dist) {

        }

        function offsetLine(start, end, dist, angle) {
            return ({
                start: {
                    x: start.x + dist*Math.cos(angle),
                    y: start.y + dist*Math.sin(angle)
                },
                end: {
                    x: end.x + dist*Math.cos(angle),
                    y: end.y + dist*Math.sin(angle)
                }
            });
        }

        function recalculateFlightPath(fp) {
            fp.start.x = parseFloat(fp.start.x);
            fp.start.y = parseFloat(fp.start.y);
            fp.end.x = parseFloat(fp.end.x);
            fp.end.y = parseFloat(fp.end.y);
            calcFlightPath = extendLineToBounds(fp);

            // the angle of the flight path and the angle perpendicular to it
            var angle = Math.atan2(fp.end.y - fp.start.y, fp.end.x - fp.start.x);
            var perpAngle = angle + (Math.PI / 2);

            var close1 = offsetLine(fp.start, fp.end,  1400, perpAngle);
            var close2 = offsetLine(fp.start, fp.end, -1400, perpAngle);
            var far1   = offsetLine(fp.start, fp.end,  2000, perpAngle);
            var far2   = offsetLine(fp.start, fp.end, -2000, perpAngle);

            close1 = extendLineToBounds(close1);
            close2 = extendLineToBounds(close2);
            far1   = extendLineToBounds(far1);
            far2   = extendLineToBounds(far2);
            
            calcLandingLines = [close1, close2, far1, far2];
        }

        function getBetween(start, end, percent) {
            start = parseInt(start);
            end = parseInt(end);
            var diff = end - start;
            var result = start + (percent * diff);
            return result;
        }

        function drawLocationsBtwn(prev, cur, percent) {
            drawMap();
            for (var i=0; i<cur.length; i++) {
                var pname = cur[i].name;
                var px = getBetween(prev[i].x, cur[i].x, percent);
                var py = getBetween(prev[i].y, cur[i].y, percent);

                if (pname === myName)
                    drawMeAt(px, py);
                else
                    drawBuddyAt(px, py);
            }
        }

        function drawLocations(locs) {
            drawMap();
            for (var i=0; i<locs.length; i++) {
                var pname = locs[i].name;
                var px = locs[i].x;
                var py = locs[i].y

                if (pname === myName)
                    drawMeAt(px, py);
                else
                    drawBuddyAt(px, py);
            }
        }

        function shouldAnimate(prev, cur) {
            // don't animate if new squads or whatever
            if (prev.length != cur.length)
                return false;
            
            // get the maximum change in in-game measurements
            var maxChg = 0;
            function compare(a, b) {
                var diff = Math.abs(a-b);
                if (diff > maxChg)
                    maxChg = diff;
            }
            for (var i=0; i<prev.length; i++) {
                compare(prev[i].x, cur[i].x);
                compare(prev[i].y, cur[i].y);
            }

            // get that measurement in on-screen distance (pixels)
            var maxChgPx = map_to_canvas(curMap, maxChg);

            return (maxChgPx > minPixelsToAnimate)
        }

        function updateLocations(locs) {
            if (prevLocs.length == 0) {
                prevLocs = locs;
            }
            var prevLocsCopy = prevLocs;
            prevLocs = locs;

            if (shouldAnimate(prevLocsCopy, locs)) {
                // jQuery.stop();
                $({foo: 0}).animate({foo: 1}, {
                    duration: 1000,
                    easing: 'linear',
                    queue: false,
                    step: function(val) {
                        drawLocationsBtwn(prevLocsCopy, locs, val);
                    }
                });
            } else {
                drawLocations(locs);
            }
        }

        function drawMeAt(x, y) {
            x = map_to_canvas(curMap, x);
            y = map_to_canvas(curMap, y);
            drawCircle(x, y, 'yellow');
        }

        function drawBuddyAt(x, y) {
            x = map_to_canvas(curMap, x);
            y = map_to_canvas(curMap, y);
            drawCircle(x, y, 'orange');
        }

        function differentFP(a, b) {
            if (a && b && a.start && a.end && b.start && b.end) {
                if (a.start.x !== b.start.x) return true;
                if (a.start.y !== b.start.y) return true;
                if (a.end.x !== a.end.y) return true;
                if (a.end.y !== b.end.y) return true;
                return false;
            }
            return true;
        }

        function poll(pollingCallback) {
            jQuery.getJSON("api/poll.php?name="+myName+"&stnum="+stnum, function(data) {
                updateLocations(data.locs);
                if (data.map) {
                    stnum = data.stnum;
                    if (curPhase !== data.phase)
                        updatePhase(data.phase);
                    if (curMap !== data.map)
                        updateMap(data.map);
                    if (differentFP(data.flightpath, curFlightPath)) {
                        updateFlightPath(data.flightpath);
                    }
                    if (data.damagedealt !== curDamageDealt)
                        updateDamageDealt(data.damagedealt);
                    if (data.kills !== curKills)
                        updateKills(data.kills);
                    if (data.headshots !== curHeadshots)
                        updateHeadshots(data.headshots);
                    if (data.maxkilldist !== curMaxKillDist)
                        updateMaxKillDist(data.maxkilldist);
                }
                pollingCallback();
            });
        }

        function drawCircle(x, y, color) {
            var centerX = x;
            var centerY = y;
            var radius = 5;

            ctx.beginPath();
            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
            ctx.fillStyle = color;
            ctx.fill();
            ctx.lineWidth = 1;
            ctx.strokeStyle = '#000';
            ctx.stroke();
        }

        function MaxOncePerSecond(requestFn){
            var beenOneSecond = false;
            var gotReturn = false;
            function onReturn() {
                gotReturn = true;
                if (beenOneSecond === true) {
                    schedulerun();
                }
            }
            function onOneSecond() {
                beenOneSecond = true;
                if (gotReturn === true) {
                    schedulerun();
                }
            }
            function schedulerun() {
                beenOneSecond = false;
                gotReturn = false;
                setTimeout(onOneSecond, 1000);
                requestFn(onReturn);
            }
            schedulerun();
        }

        function initApp() {
            MaxOncePerSecond(poll);
        }
        initApp();


        function getUrlPlayerName() {
            return window.location.search.substring(1);
        }

        function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        }



function extendLineToBounds(path) {
  var startX = path.start.x;
  var startY = path.start.y;
  var endX = path.end.x;
  var endY = path.end.y;

  var mSize = map_size(curMap);

  var offX = endX - startX;
  var offY = endY - startY;

  var dx = offX < 0 ? 0 : mSize;
  var dy = offY < 0 ? 0 : mSize;
  var py = dy;

  var sx = offX > 0 ? 0 : mSize;
  var sy = offY > 0 ? 0 : mSize;
  var ty = sy;

  if (offX === 0) {
    return {
      start: { x:startX, y:endY>startY ? 0 : mSize },
      end:   { x:startX, y:endY>startY ? mSize : 0 }
    }
  } else if (offY === 0) {
    return {
      start: { x:endX>startX ? 0 : mSize, y:starY },
      end:   { x:endX>startX ? mSize : 0, y:starY }
    }
  } else {
    dy = startY + (offY / offX) * (dx - startX);
    if (dy < 0 || dy > mSize) {
      dx = startX + (offX / offY) * (py - startY);
      dy = py;
    }

    sy = endY + (offY / offX) * (sx - endX);
    if (sy < 0 || sy > mSize) {
      sx = startX + (offX / offY) * (ty - startY);
      sy = ty;
    }
  }
  return {start: {x:sx, y:sy}, end: {x:dx, y:dy}}
}



    function rightAlign() {
        $("#col_livestats").insertBefore("#col_map");
        $("#maintable").css({
            textAlign: 'right'
        });
        $("#alignButton")
            .off('click')
            .on('click', leftAlign);
    }
    function leftAlign() {
        $("#col_map").insertBefore("#col_livestats");
        $("#maintable").css({
            textAlign: 'left'
        });
        $("#alignButton")
            .off('click')
            .on('click', rightAlign);
    }
    $("#alignButton")
            .off('click')
            .on('click', leftAlign);









    </script>
  </body>
</html>