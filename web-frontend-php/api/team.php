<?php
  header("Access-Control-Allow-Origin: *");
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }

  function writeTeam($name, $mates) {
    $db = dbConnect();
    $q=$db->prepare('
      DELETE FROM tblSquads
      WHERE name = ?
    ;');
    $q->execute(array($name));
    

    foreach ($mates as $mate) {
      $q=$db->prepare('
        INSERT INTO tblSquads
        (name, teammate)
        VALUES (?, ?)
      ;');
      $q->execute(array($name, $mate));
    }

    $db = null;
  }

  function incrementStnum($name) {
    $db = dbConnect();
    $q=$db->prepare('
      UPDATE tblStateChange
      SET statenum = (statenum + 1) % 1000
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $db = null;
  }

  function processData($data) {
    if (!isset($data['name'])) return false;
    $name = $data['name'];
    if (!isset($data['mates'])) {
      writeTeam($name, array());
      return true;
    }
    $mates = $data['mates'];
    writeTeam($name, $mates);
    incrementStnum($name);
    return true;
  }

  function go() {
    if (!processData($_REQUEST)) {
      error_log("wrong post data given!");
    }
  }

  go();
?>