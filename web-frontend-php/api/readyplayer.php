<?php
  header("Access-Control-Allow-Origin: *");
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }


  function readyPlayerInDB($name) {
    $db = dbConnect();
    $q=$db->prepare('
      INSERT OR IGNORE INTO tblLocs
      (name, x, y)
      VALUES (?, -100, -100)
    ;');
    $q->execute(array($name));
    $q=$db->prepare('
      INSERT OR IGNORE INTO tblPlayerInfo
      (name, map, phase)
      VALUES (?, "erangel", "lobby")
    ;');
    $q->execute(array($name));
    $q=$db->prepare('
      INSERT OR IGNORE INTO tblStateChange
      (name, statenum)
      VALUES (?, 1)
    ;');
    $q->execute(array($name));
    $db = null;
  }

  function processData($data) {
    if (!isset($data['name'])) return false;
    $name = $data['name'];
    readyPlayerInDB($name);
    return true;
  }

  function go() {
    if (!processData($_REQUEST)) {
      error_log("wrong post data given!");
    }
  }

  go();
?>