<?php
  header("Access-Control-Allow-Origin: *");
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }

  function writeDamageDealt($name, $maxkilldist) {
    $db = dbConnect();
    $q=$db->prepare('
      UPDATE tblPlayerInfo
      SET maxkilldist = ?
      WHERE name = ?
    ;');
    $q->execute(array($maxkilldist, $name));
    $db = null;
  }

  function incrementStnum($name) {
    $db = dbConnect();
    $q=$db->prepare('
      UPDATE tblStateChange
      SET statenum = (statenum + 1) % 1000
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $db = null;
  }

  function processData($data) {
    if (!isset($data['name'])) return false;
    if (!isset($data['maxkilldist'])) return false;
    $name = $data['name'];
    $maxkilldist = $data['maxkilldist'];
    writeDamageDealt($name, $maxkilldist);
    incrementStnum($name);
    return true;
  }

  function go() {
    if (!processData($_REQUEST)) {
      error_log("wrong post data given!");
    }
  }

  go();
?>