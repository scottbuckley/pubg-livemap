<?php
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');
  $name = $_REQUEST['name'];

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }

  function getLoc($name) {
    $db = dbConnect();
    $q=$db->prepare('
      SELECT x, y
      FROM tblLocs
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  $loc = getLoc("MrWraith");
  header('Content-Type: application/json');
  echo json_encode($loc);

?>