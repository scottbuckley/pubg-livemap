<?php
  header("Access-Control-Allow-Origin: *");
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }

  function writeLoc($name, $x, $y) {
    $db = dbConnect();
    $q=$db->prepare("
      UPDATE tblLocs
      SET x = ?, y = ?, upd = datetime('now')
      WHERE name = ?
    ;");
    $q->execute(array($x, $y, $name));
    $db = null;


    // $q=$db->prepare('
    //   UPDATE tblLocs
    //   SET x = ?, y = ?
    //   WHERE name = ?
    // ;');
    // error_log("meows");
    // if($q) {
    //   $q->execute(array($x, $y, $name));
    //   error_log("updated");
    // } else {
    //   $q=$db->prepare('
    //     INSERT INTO tblLocs (x, y, name)
    //     VALUES (?, ?, ?)
    //   ;');
    //   if ($q) {
    //     $q->execute(array($x, $y, $name));
    //     error_log("Created row for $name");
    //   } else {
    //     error_log("couldnt create row. ".print_r($db->errorInfo(), true));
    //   }
      
    // }
  }

  function processData($data) {
    if (!isset($data['name'])) return false;
    if (!isset($data['location'])) return false;
    $name = $data['name'];
    $location = json_decode($data['location'], true);
    if ($location) {
      writeLoc($name, $location['x'], $location['y']);
    } else {
      error_log(print_r($data['location'], true));
    }
    return true;
  }

  function go() {
    if (!processData($_REQUEST)) {
      error_log("wrong post data given!");
    }
  }

  go();
?>