<?php
  define('DB_PATH', $_SERVER['DOCUMENT_ROOT'] . '/db.db');
  $name  = $_REQUEST['name'];
  $stnum = $_REQUEST['stnum'];

  function dbConnect() {
    static $db;
    if (isset($db)) {
        return $db;
    } else {
      if ($db = new PDO('sqlite:'.DB_PATH)) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $db;
      } else {
          die('DBACCESSERROR');
      }
    }
  }

  function getLoc($name) {
    $db = dbConnect();
    $q=$db->prepare('
      SELECT x, y
      FROM tblLocs
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function getLocs($name) {
    $db = dbConnect();
    $q=$db->prepare("
      SELECT name, x, y FROM tblLocs WHERE name = ?
      UNION
      SELECT tblSquads.teammate, x, y
      FROM tblSquads
      INNER JOIN tblLocs ON tblSquads.teammate = tblLocs.name
      WHERE tblSquads.name = ? AND upd BETWEEN datetime('now', '-10 minutes') AND datetime('now', '+1 hour')
    ;");
    $q->execute(array($name, $name));
    $res = $q->fetchAll(PDO::FETCH_ASSOC);
    $db = null;
    return $res;
  }

  function dbGetStateNum($name) {
    $db = dbConnect();
    $q=$db->prepare('
      SELECT statenum
      FROM tblStateChange
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $num = $q->fetch(PDO::FETCH_COLUMN);
    $db = null;
    return $num;
  }

  function dbGetPlayerInfo($name) {
    $db = dbConnect();
    $q=$db->prepare('
      SELECT map, phase, flightpath, kills, damagedealt, headshots, maxkilldist
      FROM tblPlayerInfo
      WHERE name = ?
    ;');
    $q->execute(array($name));
    $res = $q->fetch(PDO::FETCH_ASSOC);
    $res["flightpath"] = json_decode($res["flightpath"]);
    $db = null;
    return $res;
  }

  function buildResponse($name, $clstnum) {
    $payload = array("locs"=>getLocs($name));
    $dbstnum = dbGetStateNum($name);
    if ($dbstnum != $clstnum) {
      $payload = array_merge($payload, dbGetPlayerInfo($name), array("stnum"=>$dbstnum));
    }
    echo json_encode($payload);
  }

  header('Content-Type: application/json');
  buildResponse($name, $stnum);

?>